import { Injectable } from '@nestjs/common';
import { Connection, Not, Repository } from 'typeorm';
import { DeleteResult } from 'typeorm/query-builder/result/DeleteResult';
import { AccountService } from '../account/account.service';
import { CopyPerson } from './entities/copy-person.entity';
import { OrderEntity } from '../orders/entities/order.entity';

@Injectable()
export class CopyPeopleService {
  private readonly copyPeopleRepository: Repository<CopyPerson>;
  private readonly orderRepository: Repository<OrderEntity>;

  constructor(connection: Connection, private readonly accountService: AccountService) {
    this.copyPeopleRepository = connection.getRepository(CopyPerson);
    this.orderRepository = connection.getRepository(OrderEntity);
  }

  async copyPerson(createCopyPerson): Promise<CopyPerson | { status: string }> {
    const { copyPersonId, accountId, ...data } = createCopyPerson;


    const copyPersonAccount = await this.accountService.findOne(copyPersonId, ['orders', 'orders.account', 'orders.orderType', 'orders.tradeType', 'orders.assetType']);

    const check = await this.copyPeopleRepository.find({
      where: {
        copyAccount: copyPersonAccount.id,
        account: accountId,
      },
    });
    if (check.length > 0) return { status: 'already exist' };
    const account = await this.accountService.findOne(accountId);
    const res = [];
    for (const o of copyPersonAccount.orders) {
      o.invested = data.invested * (o.investedPercentage / 100);
      o.account = account;
      delete o.id;
      res.push(o);
    }

    const entity = { account: accountId, ...data, copyAccount: copyPersonAccount };
    await this.orderRepository.save(res);
    console.log(entity);
    return this.copyPeopleRepository.save(entity);
  }

  async copyPersonSaveOrder(accountId, order): Promise<CopyPerson[]> {
    const { investedPercentage, id, ...data } = order;
    const person = await this.copyPeopleRepository.find({
      where: {
        copyAccount: accountId,
      },
      relations: ['account'],
    });

    await Promise.all(person.map(async (p) => {
      const newOrder = await this.orderRepository.create({
        ...data,
        invested: p.invested * (investedPercentage / 100),
        account: p.account,
      });
      return this.orderRepository.save(newOrder);
    }));

    return person;
  }

  findOrdersByAccount(accountId: number): Promise<CopyPerson[]> {
    return this.copyPeopleRepository.find({
      where: { person: accountId },
      relations: ['copyPerson'],
    });
  }

  copiers(accountId: number):Promise<CopyPerson[]> {
    return this.copyPeopleRepository
      .createQueryBuilder('copyPerson')
      .where(`personId = ${accountId}`)
      .getMany();
  }

  findOne(id: number, accountId: number): Promise<CopyPerson> {
    return this.copyPeopleRepository.findOne(id, {
      relations: ['copyPerson', 'copyPerson.orders'],
      where: { person: accountId },
    });
  }

  remove(id: number): Promise<DeleteResult> {
    return this.copyPeopleRepository.delete(id);
  }
}
