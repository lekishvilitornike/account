import { Module } from '@nestjs/common';
import { CopyPeopleService } from './copy-people.service';
import { CopyPeopleController } from './copy-people.controller';
import { AccountModule } from '../account/account.module';

@Module({
  controllers: [CopyPeopleController],
  providers: [CopyPeopleService],
  imports: [AccountModule],
  exports: [CopyPeopleService],
})
export class CopyPeopleModule {
}
