import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, Unique } from 'typeorm';
import { Account } from '../../account/entities/account.entity';

@Unique('copyPersonFields', ['account', 'copyAccount'])
@Entity()
export class CopyPerson {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  status: string;

  @Column({ type: 'float' })
  invested: number;

  @Column({ type: 'float' })
  copyStopLoss: number;

  @ManyToOne(() => Account, account => account.person, { nullable: false })
  account: Account;

  @ManyToOne(() => Account, account => account.copyPerson, { nullable: false })
  copyAccount: Account;

  @CreateDateColumn()
  createdAt: Date;
}
