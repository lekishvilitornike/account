import { Controller, Get, Post, Body, Put, Param, Delete, Query } from '@nestjs/common';
import { CopyPeopleService } from './copy-people.service';
import { DeleteResult } from 'typeorm/query-builder/result/DeleteResult';
import { CopyPerson } from './entities/copy-person.entity';

@Controller('copyPeople')
export class CopyPeopleController {
  constructor(private readonly copyPeopleService: CopyPeopleService) {
  }

  @Get('')
  home() {
    return this.copyPeopleService.copyPersonSaveOrder(1, '');
  }

  @Post()
  create(@Body() createCopyPerson): Promise<CopyPerson | { status: string }> {
    return this.copyPeopleService.copyPerson(createCopyPerson);
  }

  @Get(':id')
  findOne(@Param('id') id: number, @Query('accountId') accountId: number): Promise<CopyPerson> {
    return this.copyPeopleService.findOne(id, accountId);
  }

  // @Put(':id')
  // update(@Param('id') id: string, @Body() updateCopyPersonDto: UpdateCopyPersonDto) {
  //   return this.copyPeopleService.update(+id, updateCopyPersonDto);
  // }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<DeleteResult> {
    return this.copyPeopleService.remove(+id);
  }
}
