import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AccountModule } from './account/account.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AssetModule } from './asset/asset.module';
import { ElasticsearchRepository } from '@gamma/elasticsearch-client';
import { connectionOptions } from './config/connection.config';
import { WalletModule } from './wallet/wallet.module';
import { CacheModule } from './cache/cache.module';
import { OrderModule } from './orders/order.module';
import { CopyPeopleModule } from './copy-people/copy-people.module';
import { FilesModule } from './files/files.module';

@Module({
  imports: [
    AccountModule,
    TypeOrmModule.forRoot(connectionOptions),
    AssetModule,
    WalletModule,
    CacheModule,
    OrderModule,
    CopyPeopleModule,
    FilesModule,
  ],
  controllers: [AppController],
  providers: [AppService, ElasticsearchRepository],
})
export class AppModule {
}
