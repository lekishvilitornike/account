import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Account } from '../../account/entities/account.entity';
import { FileEntity } from './file.entity';

@Entity()
export class FileTypeEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => FileEntity, file => file.fileType)
  files: FileEntity[];
}