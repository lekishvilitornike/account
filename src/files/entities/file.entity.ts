import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Account } from '../../account/entities/account.entity';
import { AccountType } from '../../account/entities/accountType.entity';
import { FileTypeEntity } from './fileType.entity';

@Entity()
export class FileEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Account, type => type.files)
  account: Account;

  @Column()
  key: string;

  @ManyToOne(() => FileTypeEntity, type => type.files)
  fileType: FileTypeEntity;
}