import { Body, Controller, Get, Post, Query, Req, Res, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FilesService } from './files.service';
import { FileInterceptor } from '@nestjs/platform-express';


@Controller('files')
export class FilesController {

  constructor(private readonly filesService: FilesService) {
  }

  @Get('')
  async getFiles(@Query('accountId') accountId: number) {
    return this.filesService.getAccountFiles(1);
  }

  @Post('upload')
  async uploadFile(@Body() body) {
    await this.filesService.uploadFile(body);
  }
}
