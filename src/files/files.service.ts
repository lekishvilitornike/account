import { Injectable, NotFoundException } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { FileEntity } from './entities/file.entity';
import * as aws from 'aws-sdk';
import { AWS_S3_BUCKET_NAME } from '../config/aws.config';
import { v4 as uuid } from 'uuid';
import { FileTypeEntity } from './entities/fileType.entity';

@Injectable()
export class FilesService {
  private readonly fileRepository: Repository<FileEntity>;
  private readonly fileTypeRepository: Repository<FileTypeEntity>;

  constructor(connection: Connection) {
    this.fileRepository = connection.getRepository(FileEntity);
    this.fileTypeRepository = connection.getRepository(FileTypeEntity);
  }

  async uploadFile(body) {
    const { file, data } = body;
    const s3 = this.createS3();
    const fileType = await this.getTypeByName(data.fileType);

    const fileName = `${file.originalname.split('.')[1]}`;
    const uploadResult = await s3.upload({
      Bucket: AWS_S3_BUCKET_NAME,
      Body: Buffer.from(file.buffer, "binary"),
      Key: `${uuid()}_document_${fileType.name}_${data.accountId}.${fileName}`,
      ContentType: file.mimetype,
    }).promise();

    return this.fileRepository.save({
      key: uploadResult.Key,
      account: data.accountId,
      fileType,
    });
  }

  getTypeByName(name: string) {
    return this.fileTypeRepository.find({ where: { name } }).then(x => x[0]);
  }

  async getAccountFiles(accountId: number) {
    const files = await this.fileRepository.find({ where: { account: accountId }, relations: ['fileType'] });
    if (files) {
      return Promise.all(
        files.map(async (file) => {
          const url = await this.generatePresignedUrl(file.key);
          return {
            ...file,
            url,
          };
        }),
      );
    }
    throw new NotFoundException('files with this account id does not exist');
  }

  private generatePresignedUrl(key: string) {
    const s3 = this.createS3();

    return s3.getSignedUrlPromise('getObject', {
      Bucket: AWS_S3_BUCKET_NAME,
      Key: key,
    });
  }

  async generateStreamFile() {
    const s3 = this.createS3();
    const files = await this.fileRepository.findOne(2);
    const stream = await s3.getObject({
      Bucket: AWS_S3_BUCKET_NAME,
      Key: files.key,
    })
      .createReadStream();
    return {
      stream,
      info: files,
    };
  }


  private createS3() {
    return new aws.S3();
  }
}