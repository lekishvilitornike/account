import { Injectable } from '@nestjs/common';
import { ElasticsearchClient, ElasticsearchRepository } from '@gamma/elasticsearch-client';
import { elasticSearchOptions } from './config/elasticSearch.config';

@Injectable()
export class AppService {
  constructor(private esRepo: ElasticsearchRepository<any>) {
    this.esRepo = new ElasticsearchClient({
      node: elasticSearchOptions.node,
    }).getRepository('orders');
  }

  async getHello(): Promise<string> {

    const builder = await this.esRepo.createQueryBuilder();
    console.log(builder.params);
    return 'Hello World!';
  }
}
