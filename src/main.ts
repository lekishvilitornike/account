import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { awsConfig } from './config/aws.config';
import { json } from 'express';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    allowedHeaders: '*',
    origin: '*',
  });
  const port = process.env.PORT || 3002;
  awsConfig();
  app.use(json({ limit: '50mb' }));
  await app.listen(port, () => {
    console.log(`Listening to port: ${port}`);
  });
}

bootstrap();
