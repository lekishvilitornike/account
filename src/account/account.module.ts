import {  Module } from '@nestjs/common';
import { AccountService } from './account.service';
import { AccountController } from './account.controller';
import { FilesService } from '../files/files.service';

@Module({
  controllers: [AccountController],
  providers: [AccountService, FilesService],
  exports: [AccountService],
})
export class AccountModule {
}

