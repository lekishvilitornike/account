import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { AccountService } from './account.service';
import { FilesService } from '../files/files.service';
import { Account } from './entities/account.entity';
import { DeleteResult } from 'typeorm/query-builder/result/DeleteResult';
import { UpdateResult } from 'typeorm/query-builder/result/UpdateResult';

@Controller('account')
export class AccountController {
  constructor(private readonly accountService: AccountService, private readonly awsService: FilesService) {
  }

  @Get('me')
  getAccount(@Query('accountId') accountId: number): Promise<Account> {
    return this.accountService.getOne(accountId);
  }

  @Post()
  create(@Body() createAccount): Promise<Account> {
    return this.accountService.create(createAccount);
  }

  @Get('all')
  getAll(@Query() query): Promise<Account[]> {
    return this.accountService.findAll(query);
  }

  @Get('copyPersonList')
  findAllAccount(@Query('accountId') accountId: number): Promise<Account[]> {
    return this.accountService.copyPersonList(accountId);
  }

  @Get('subAccounts')
  findAllSubAccount(@Query('accountId') accountId: number): Promise<Account[]> {
    return this.accountService.findAllSubAccount(accountId);
  }


  @Put(':id')
  update(@Param('id') id: string, @Body() updateAccount): Promise<UpdateResult> {
    return this.accountService.update(+id, updateAccount);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<DeleteResult> {
    return this.accountService.remove(+id);
  }
}
