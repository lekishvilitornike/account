import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { Account } from './entities/account.entity';
import { DeleteResult } from 'typeorm/query-builder/result/DeleteResult';
import { UpdateResult } from 'typeorm/query-builder/result/UpdateResult';
import { AccountType } from './entities/accountType.entity';
import { FilesService } from '../files/files.service';

@Injectable()
export class AccountService {
  private readonly accountRepository: Repository<Account>;
  private readonly accountTypeRepository: Repository<AccountType>;

  constructor(connection: Connection, private readonly awsService: FilesService) {
    this.accountRepository = connection.getRepository(Account);
    this.accountTypeRepository = connection.getRepository(AccountType);
  }

  async create(createAccount): Promise<Account> {
    createAccount.accountType = await this.findTypeByName(createAccount.accountType);
    return this.accountRepository.save(createAccount);
  }

  async getOne(accountId: number): Promise<Account> {
    const account = await this.accountRepository.findOne(accountId, {
      where: { accountType: 1 },
      relations: ['subAccount', 'wallets', 'wallets.currency', 'assetToAccount', 'assetToAccount.orders'],
    });
    return account;
  }

  findAll(query): Promise<Account[]> {
    return this.accountRepository.find({
      where: { accountType: 1 },
      take: query.take,
      skip: query.skip,
      order: { [query.orderBy || 'createdAt']: query.order || 'DESC' },
      relations: ['subAccount', 'wallets', 'wallets.currency', 'assetToAccount', 'assetToAccount.orders'],
    });
  }

  copyPersonList(accountId: number): Promise<Account[]> {
    return this.accountRepository
      .createQueryBuilder('account')
      .where(`account.id not in(select "copyPersonId" from copy_person where "personId" = ${accountId}) and account.id != ${accountId}`)
      .leftJoinAndSelect('account.copyPerson', 'copyPerson')
      .getMany();
  }


  findOne(id: number, relations?: string[]): Promise<Account> {
    let relation;
    if (relations) relation = { relations };
    return this.accountRepository.findOne(id, relation);
  }

  findByName(name: string, relations?: string[]): Promise<Account> {
    return this.accountRepository.findOneOrFail({ where: { name }, relations });
  }

  findTypeByName(name: string): Promise<AccountType> {
    return this.accountTypeRepository.find({ where: { name } }).then(x => x[0]);
  }

  findAllSubAccount(accountId): Promise<Account[]> {
    return this.accountRepository.find({ where: { accountType: 2, userId: accountId } });
  }

  update(id: number, updateAccount): Promise<UpdateResult> {
    return this.accountRepository.update(id, updateAccount);
  }

  remove(id: number): Promise<DeleteResult> {
    return this.accountRepository.delete(id);
  }
}
