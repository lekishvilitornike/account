import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn, UpdateDateColumn,
} from 'typeorm';
import { AccountAsset } from '../../asset/entities/AccountAsset.entity';
import { AccountType } from './accountType.entity';
import { OrderEntity } from '../../orders/entities/order.entity';
import { Wallet } from '../../wallet/entities/wallet.entity';
import { CopyPerson } from '../../copy-people/entities/copy-person.entity';
import { FileEntity } from '../../files/entities/file.entity';

@Entity()
export class Account {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'float', default: 0 })
  totalAllocated: number;

  @Column({ type: 'float', default: 0 })
  profit: number;

  @OneToMany(() => Wallet, wallet => wallet.account)
  wallets: Wallet[];

  @OneToMany(() => OrderEntity, orders => orders.account)
  orders: OrderEntity[];

  @ManyToOne(() => AccountType, type => type.accounts)
  accountType: AccountType;

  @ManyToOne(() => Account, account => account.subAccount)
  account: Account;

  @OneToMany(() => FileEntity, files => files.account)
  files: FileEntity[];

  @OneToMany(() => Account, account => account.account)
  subAccount: Account[];

  @OneToMany(() => AccountAsset, assetToAccount => assetToAccount.account)
  assetToAccount: AccountAsset[];

  @OneToMany(() => CopyPerson, copyPerson => copyPerson.account)
  person: CopyPerson[];

  @OneToMany(() => CopyPerson, copyPerson => copyPerson.copyAccount)
  copyPerson: CopyPerson[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
