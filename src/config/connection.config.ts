import { OrderEntity } from '../orders/entities/order.entity';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { Account } from '../account/entities/account.entity';
import { AccountType } from '../account/entities/accountType.entity';
import { Asset } from '../asset/entities/asset.entity';
import { AssetType } from '../asset/entities/assetType.entity';
import { AccountAsset } from '../asset/entities/AccountAsset.entity';
import { OrderType } from '../orders/entities/orderType.entity';
import { TradeType } from '../orders/entities/tradeType.entity';
import { Wallet } from '../wallet/entities/wallet.entity';
import { AssetToAccountTypes } from '../asset/entities/assetToAccountTypes.entity';
import { CopyPerson } from '../copy-people/entities/copy-person.entity';
import { CurrencyType } from '../wallet/entities/currencyType.entity';
import { FileEntity } from '../files/entities/file.entity';
import { FileTypeEntity } from '../files/entities/fileType.entity';


export const connectionOptions: TypeOrmModuleOptions = {
  type: 'postgres',
  host: process.env.POSTGRES_HOST || 'localhost',
  port: 5432,
  username: process.env.POSTGRES_USERNAME || 'root',
  password: process.env.POSTGRES_PASSWORD || 'root',
  database: process.env.POSTGRES_DATABASE || 'users',
  entities: [
    Account,
    AccountType,
    Asset,
    AssetType,
    AccountAsset,
    OrderEntity,
    OrderType,
    TradeType,
    Wallet,
    AssetToAccountTypes,
    CopyPerson,
    CurrencyType,
    FileEntity,
    FileTypeEntity
  ],
  synchronize: true,
  // migrationsRun: true,
  migrations: [],
  cli: {
    'migrationsDir': './src/migration',
  },
};
//
