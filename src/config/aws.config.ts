import { config } from 'aws-sdk';

export const AWS_S3_BUCKET_NAME = process.env.BUCKET_NAME || '';
const accessKeyId = process.env.ACCESS_KEY_ID || '';
const secretAccessKey = process.env.ACCESS_KEY || '';
const awsRegion = process.env.AWS_REGION || '';


export const awsConfig = () => {
  return config.update({
    accessKeyId: accessKeyId,
    secretAccessKey: secretAccessKey,
    region: awsRegion,
  });
};