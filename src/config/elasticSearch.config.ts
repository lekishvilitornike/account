export const elasticSearchOptions = {
  node: String(process.env.ES_HOST || 'http://localhost:9200'),
  index: String(process.env.ORDERS_ES_INDEX || 'orders'),
};
