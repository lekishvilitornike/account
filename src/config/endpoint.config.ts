export const endpoints = {
  iex: {
    api: {
      sandbox: process.env.iexSandBox || '',
      sse: process.env.iexSse || '',
      baseUrl: process.env.iexBaseUrl || '',
    },
    token: {
      sandbox:
        process.env.iexSandBoxToken || '',
      premium:
        process.env.iexPremiumToken || '',
    },
  },
  fmp: {
    api: {
      dev: '',
    },
    token: {
      dev: process.env.fmGToken || '',
    },
  },
};
