import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { TradeType } from '../entities/tradeType.entity';

@Injectable()
export class TraderTypeService {
  private readonly tradeTypeRepository: Repository<TradeType>;

  constructor(connection: Connection) {
    this.tradeTypeRepository = connection.getRepository(TradeType);
  }

  getTypeByName(name: string): Promise<TradeType> {
    return this.tradeTypeRepository.find({ where: { name } }).then(x => x[0]);
  }
}
