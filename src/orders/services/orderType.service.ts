import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { OrderType } from '../entities/orderType.entity';

@Injectable()
export class OrderTypeService {
  private readonly orderTypeRepository: Repository<OrderType>;

  constructor(connection: Connection) {
    this.orderTypeRepository = connection.getRepository(OrderType);
  }

  getTypeByName(name: string): Promise<OrderType> {
    return this.orderTypeRepository.find({ where: { name } }).then(x => x[0]);
  }
}
