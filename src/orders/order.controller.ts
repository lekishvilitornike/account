import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { OrderService } from './order.service';
import { OrderEntity } from './entities/order.entity';

@Controller('orders')
export class OrderController {
  constructor(private readonly orderService: OrderService) {
  }

  @Get('')
  getOrders(@Query() query): Promise<OrderEntity[]> {
    return this.orderService.getOrders(query);
  }

  @Post('save')
  async saveOrders(@Body() body): Promise<OrderEntity> {
    return this.orderService.saveOrders(body);
  }

  @Get('test')
  test() {
    return this.orderService.checkOrders();
  }

  @Get('equity')
  equity(@Query('accountId') accountId: number) {
    return this.orderService.accountEquity(accountId);
  }
}
