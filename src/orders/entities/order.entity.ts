import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne, OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Account } from '../../account/entities/account.entity';
import { OrderType } from './orderType.entity';
import { TradeType } from './tradeType.entity';
import { AccountAsset } from '../../asset/entities/AccountAsset.entity';
import { Asset } from '../../asset/entities/asset.entity';


@Entity()
export class OrderEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'float' })
  invested: number;

  @Column({ type: 'float', nullable: true })
  unit: number;

  @Column({ nullable: true, type: 'float' })
  investedPercentage: number;

  @Column({ nullable: true, type: 'float' })
  openPrice: number;

  @Column({ nullable: true, type: 'float' })
  closePrice: number;

  @Column({ nullable: true, type: 'float' })
  stopLoss: number;

  @Column({ nullable: true, type: 'float' })
  takeProfit: number;

  @Column({ nullable: true, type: 'float' })
  orderPrice: number;

  @Column({ nullable: true, type: 'float' })
  profit: number;

  @Column({ nullable: true, type: 'float' })
  profitPercentage: number;

  @Column({ nullable: true, type: 'float' })
  openPriceTime: Date;

  @Column({ nullable: true, type: 'float' })
  closePriceTime: Date;

  @Column({ nullable: true })
  risk: number;

  @Column()
  status: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne(() => AccountAsset, assetToAccount => assetToAccount.asset)
  orderToAsset: AccountAsset;

  @ManyToOne(() => Account, account => account.orders)
  account: Account;

  @ManyToOne(() => OrderType, orderType => orderType.orders)
  orderType: OrderType;

  @ManyToOne(() => TradeType, tradeType => tradeType.orders)
  tradeType: TradeType;

  @ManyToOne(() => Asset, type => type.orders)
  assetType: Asset;
}
