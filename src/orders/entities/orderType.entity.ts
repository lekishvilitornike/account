import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { OrderEntity } from './order.entity';

@Entity()
export class OrderType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => OrderEntity, orders => orders.orderType)
  orders: OrderEntity[];

  @CreateDateColumn()
  createdAt: Date;
}
