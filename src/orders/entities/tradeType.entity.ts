import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { OrderEntity } from './order.entity';

@Entity()
export class TradeType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => OrderEntity, orders => orders.tradeType)
  orders: OrderEntity[];

  @CreateDateColumn()
  createdAt: Date;
}
