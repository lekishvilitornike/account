import { HttpModule, Module } from '@nestjs/common';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';
import { AssetModule } from '../asset/asset.module';
import { AccountModule } from '../account/account.module';
import { OrderTypeService } from './services/orderType.service';
import { TraderTypeService } from './services/traderType.service';
import { WalletModule } from '../wallet/wallet.module';
import { CopyPeopleService } from '../copy-people/copy-people.service';


@Module({
  imports: [
    HttpModule,
    AssetModule,
    AccountModule,
    WalletModule,
  ],
  controllers: [OrderController],
  providers: [OrderService, OrderTypeService, CopyPeopleService, TraderTypeService],
  exports: [OrderService],
})
export class OrderModule {
}
