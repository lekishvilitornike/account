import { HttpService, Injectable } from '@nestjs/common';
import {
  ElasticsearchClient,
  ElasticsearchRepository,
} from '@gamma/elasticsearch-client';
import { Connection, Repository } from 'typeorm';
import { elasticSearchOptions } from '../config/elasticSearch.config';
import { endpoints } from '../config/endpoint.config';
import { OrderEntity } from './entities/order.entity';
import { OrderTypeService } from './services/orderType.service';
import { TraderTypeService } from './services/traderType.service';
import { AccountService } from '../account/account.service';
import { Wallet } from '../wallet/entities/wallet.entity';
import { AssetService } from '../asset/asset.service';
import { WalletService } from '../wallet/wallet.service';
import { AccountAssetService } from '../asset/services/accountAsset.service';
import { Cron } from '@nestjs/schedule';
import { CopyPeopleService } from '../copy-people/copy-people.service';


@Injectable()
export class OrderService {
  private readonly iexEndpoint: string;
  private readonly iexToken: string;
  private readonly fmpEndpoint: string;
  private readonly fmpToken: string;
  private readonly orderRepository: Repository<OrderEntity>;
  private readonly walletRepository: Repository<Wallet>;
  private readonly esRepo: ElasticsearchRepository<any>;

  constructor(
    private readonly httpService: HttpService,
    private readonly orderTypeService: OrderTypeService,
    private readonly tradeTypeService: TraderTypeService,
    private readonly accountService: AccountService,
    private readonly assetService: AssetService,
    private readonly walletService: WalletService,
    private readonly accountAssetService: AccountAssetService,
    private readonly copyPerson: CopyPeopleService,
    connection: Connection,
  ) {
    this.esRepo = new ElasticsearchClient({
      node: elasticSearchOptions.node,
    }).getRepository(elasticSearchOptions.index);
    this.iexEndpoint = endpoints.iex.api.baseUrl;
    this.iexToken = endpoints.iex.token.sandbox;

    this.fmpEndpoint = endpoints.fmp.api.dev;
    this.fmpToken = endpoints.fmp.token.dev;

    this.orderRepository = connection.getRepository(OrderEntity);
    this.walletRepository = connection.getRepository(Wallet);
  }

  async saveOrders(data): Promise<OrderEntity | any> {
    const { tradeType, orderType, assetName, walletId, currency = 'USD' } = data;
    const accountWallet = await this.walletService.findOne(walletId, null, { account: data.accountId });

    // data.invested = await this.iexConvertAmount({ fromSy: currency }, data.invested).then(x => x[0].amount); TODO ??????
    // console.log(data.invested);
    if (accountWallet.balance < data.invested) return { msg: 'not enough fund' };

    const asset = await this.assetService.findIdByName(assetName);
    const accountAsset = await this.accountAssetService.updateOrCreateData(
      {
        ...data,
        balance: accountWallet.balance,
        account: data.accountId,
        asset: asset.id,
      });

    const { res, wallet } = this.order(data, accountWallet);
    res.tradeType = await this.tradeTypeService.getTypeByName(tradeType);
    res.orderType = await this.orderTypeService.getTypeByName(orderType);
    res.assetType = asset;
    res.orderToAsset = accountAsset;
    res.account = data.accountId;
    await this.walletService.update(wallet.id, wallet);
    const order = await this.orderRepository.save(res);
    this.copyPerson.copyPersonSaveOrder(data.accountId, order);

    return order;
  }

  private order(res, wallet) {
    const { orderType, tradeType } = res;
    if (tradeType == 'trade') {
      if (orderType == 'buy') {
        res.profit = this.buy(res);
        wallet.balance += res.profit;
        res.status = 'closed';
        res.openPrice = res.orderPrice;
      } else if (orderType == 'sell') {
        res.profit = this.sell(res);
        wallet.balance += res.profit;
        res.status = 'closed';
        res.openPrice = res.orderPrice;
      }
    } else {
      res.status = 'pending';
    }
    res.investedPercentage = (res.invested * 100) / wallet.balance;
    // wallet.balance -= Number(res.invested); TODO გასარკვევია

    return { res, wallet };
  }

  async getOrders(data: any): Promise<OrderEntity[]> {
    const { accountId, fromDate, toDate, limit = 20, offset = 0, orderAsset } = data;
    const order = await this.orderRepository
      .createQueryBuilder('order')
      .where({ 'account': accountId })
      .take(limit)
      .offset(offset);

    // const orderAssetId = await this.accountAssetService.
    if (orderAsset) order.andWhere(`order.orderToAssetId = ${orderAsset}`);
    if (fromDate) {
      const fromD = new Date(fromDate).toISOString();
      order.andWhere(`order.createdAt >= '${fromD}'`);
    }

    if (toDate) {
      const toD = new Date(toDate).toISOString();
      console.log(toD);
      order.andWhere(`order.createdAt <= '${toD}'`);
    }


    return order.getMany().catch(e => e);
  }

  // @Cron('* * * * *')
  async checkOrders() {
    const orders: any = await this.orderRepository.find({
      where: { status: 'open' },
      relations: ['orderType', 'assetType', 'assetType.assetType'],
    });

    for (const order of orders) {
      const price = order.assetType.assetType.name == 'stock' || order.assetType.assetType.name == 'crypto' ? await
        this.getIexStockPrice(order.assetType.name) : await this.getFmpPrices(order.assetType.name);
      const bidPrice = price.iexBidPrice || price.bid;
      const askPrice = price.iexAskPrice || price.askPrice;
      const buy = order.orderType.name == 'buy' && this.buyTakeProfitOrStopLoss(bidPrice, order);
      const sell = order.orderType.name == 'sell' && this.sellTakeProfitOrStopLoss(askPrice, order);
      const data = { openPrice: order.openPrice, unit: order.unit, bidPrice, askPrice };

      const profit = buy ? await this.buy(data) : sell ? await this.sell(data) : null;

      if (profit) {
        order.profit = profit;
        await this.orderRepository.update(order._id, order);
      }
    }
  }

  @Cron('* * * * * *')
  async checkPendingOrder() {
    const orders: any = await this.orderRepository.find({
      where: { status: 'pending' },
      relations: ['orderType', 'tradeType', 'assetType', 'assetType.assetType'],
    });

    for (const order of orders) {
      const orderPrice = Number(order.orderPrice);

      const type = order.orderType.name
        .replace(/([a-z])([A-Z])/g, '$1 ')
        .slice(0, 4)
        .trim();
      // const price = order.assetType.assetType.name == 'stock' || order.assetType.assetType.name == 'crypto' ? await
      // this.getIexStockPrice(order.symbol) : await this.getFmpPrices(order.symbol);
      const price = 54.62;
      console.log(order);
      const check =
        (order.orderType.name == 'buyStop' && price >= orderPrice) ||
        (order.orderType.name == 'buyLimit' && price <= orderPrice) ||
        (order.orderType.name == 'sellStop' && price <= orderPrice) ||
        (order.orderType.name == 'sellLimit' && price >= orderPrice);
      check ? await this.orderRepository.update(order.id, {
        orderType: await this.orderTypeService.getTypeByName(type),
        status: 'open',
        openPrice: price,
      }) : null;
    }
    return;
  }

  private async iexConvertAmount(symbols: { fromSy: string, toSy?: string }, amount: number) {
    const { fromSy, toSy = 'USD' } = symbols;
    const params = {
      token: this.iexToken,
      symbols: `${fromSy}${toSy}`,
      amount,
    };
    const url = `${this.iexEndpoint}/fx/convert`;
    return this.httpService.get(url, { params })
      .toPromise().then(x => x.data);
  }

  private async getIexStockPrice(symbol) {
    const params = {
      token: this.iexToken,
    };
    const url = `${this.iexEndpoint}/stock/${symbol}/quote`;
    return this.httpService.get(url, { params })
      .toPromise().then(x => x.data);
  }

  private async getFmpPrices(symbol) {
    const params = {
      apikey: this.fmpToken,
    };
    const url = `${this.fmpEndpoint}/fx/${symbol}`;
    console.log(url + '?apikey=' + params.apikey);
    return this.httpService.get(url, { params })
      .toPromise().then(x => x.data[0]);
  }

  private buy(order: any): number {
    return order.bidPrice - order.openPrice * order.unit;
  }

  private sell(order: any): number {
    return (order.askPrice - order.openPrice) * -1 * order.unit;
  }

  private buyTakeProfitOrStopLoss(bidPrice, order): boolean {
    return (bidPrice <= ((-order.stopLoss / order.unit) + order.openPrice) || bidPrice >= (order.takeProfit / order.unit) + order.openPrice);
  }

  private sellTakeProfitOrStopLoss(askPrice, order): boolean {
    return (askPrice >= (order.takeProfit / order.unit) + order.openPrice) || (askPrice <= ((-order.stopLoss / order.unit) + order.openPrice));
  }


  async accountEquity(accountId: number) {
    const { profit, invested } = await this.orderRepository
      .createQueryBuilder('order')
      .where({ account: accountId })
      .select('sum(order.profit)', 'profit')
      .addSelect(' sum(invested)', 'invested')
      .getRawOne();

    const { balance } = await this.walletRepository
      .createQueryBuilder('wallet')
      .where({ account: accountId })
      .leftJoinAndSelect('wallet.currency', 'currency')
      .andWhere(`currency.name = 'USD'`)
      .getOne();

    const equity = balance + profit + invested;

    return { balance, profit, invested, equity };
  }

}
