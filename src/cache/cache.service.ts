import { Injectable } from '@nestjs/common';
import { promisify } from 'util';
import { serialize } from 'class-transformer';
import * as redis from 'redis';

@Injectable()
export class CacheService {
  client: any;
  private getAsync: any;

  constructor() {
    const config = {
      host: String(process.env.REDIS_HOST || 'localhost'),
      port: Number(process.env.REDIS_PORT || 6379),
      // password: String(process.env.REDIS_PASSWORD || ''),
    };
    this.client = redis.createClient(config);
    this.getAsync = promisify(this.client.get).bind(this.client);
  }

  /*
  *
  * Get an item from the cache, or store the default value.
  *
  * */
  async remember(key: any, minutes: any, cb: any) {
    let value = await this.get(key);

    if (value) {
      return value;
    }

    this.put(key, value = await cb(), minutes);

    return value;
  }

  /*
  *
  * Retrieve an item from the cache by key.
  *
  * */
  async get(key: any) {
    return await this.getAsync(key);
  }

  /*
  *
  * Store an item in the cache.
  *
  *  */
  put(key: any, value: any, minutes: any = null, expire: any = false) {
    if (value) {
      expire && minutes ? this.client.setex(key, minutes, serialize(value)) : this.client.set(key, serialize(value));
    }
  }
}
