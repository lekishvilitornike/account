import { Module } from '@nestjs/common';
import { CacheService } from './cache.service';


@Module({
  imports: [],
  exports: [CacheService],
  providers: [CacheService],
  controllers: [],
})
export class CacheModule {

}
