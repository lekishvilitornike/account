import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Wallet } from './wallet.entity';

@Entity()
export class CurrencyType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => Wallet, wallets => wallets.currency)
  wallets: Wallet[];

  @CreateDateColumn()
  createdAt: Date;
}
