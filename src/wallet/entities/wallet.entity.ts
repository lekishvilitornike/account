import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, Unique, UpdateDateColumn } from 'typeorm';
import { Account } from '../../account/entities/account.entity';
import { CurrencyType } from './currencyType.entity';


@Unique('walletCurrency', ['account', 'currency'])
@Entity()
export class Wallet {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ default: 0, type: 'float' })
  balance: number;

  @Column({ default: false })
  main: boolean;

  @ManyToOne(() => Account, account => account.wallets)
  account: Account;

  @ManyToOne(() => CurrencyType, type => type.wallets)
  currency: CurrencyType;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
