import { Injectable } from '@nestjs/common';
import { Connection, DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Wallet } from './entities/wallet.entity';
import { CurrencyType } from './entities/currencyType.entity';

@Injectable()
export class WalletService {
  private readonly walletRepository: Repository<Wallet>;
  private readonly currencyRepository: Repository<CurrencyType>;

  constructor(connection: Connection) {
    this.walletRepository = connection.getRepository(Wallet);
    this.currencyRepository = connection.getRepository(CurrencyType);
  }

  async create(wallet): Promise<Wallet> {
    wallet.currency = await this.getCurrencyTypeByName(wallet.currency);
    return this.walletRepository.save(wallet);
  }

  findAll(accountId): Promise<Wallet[]> {
    return this.walletRepository.find({ where: { account: accountId } });
  }

  getCurrencyTypeByName(name: string): Promise<CurrencyType> {
    return this.currencyRepository.find({ where: { name } }).then(x => x[0]);
  }

  async transferMoney(data): Promise<Wallet> {
    const wallet = await this.walletRepository.findOne(data.walletId, { where: { account: data.accountId } });

    wallet.balance += Number(data.amount);
    return this.walletRepository.save(wallet);
  }

  findOne(id: number, relations?: [], conditions?: any): Promise<Wallet> {
    let con;
    if (conditions) con = { where: { ...conditions } };
    return this.walletRepository.findOne(id, { relations, ...con });
  }

  async update(id: number, updateWallet): Promise<UpdateResult> {
    return this.walletRepository.update(id, updateWallet);
  }

  remove(id: number): Promise<DeleteResult> {
    return this.walletRepository.delete(id);
  }
}
