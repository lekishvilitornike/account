import { Controller, Get, Post, Body, Put, Param, Delete, Query } from '@nestjs/common';
import { WalletService } from './wallet.service';
import { Wallet } from './entities/wallet.entity';
import { DeleteResult, UpdateResult } from 'typeorm';

@Controller('wallet')
export class WalletController {
  constructor(private readonly walletService: WalletService) {
  }

  @Post('create')
  create(@Body() createWallet): Promise<Wallet> {
    return this.walletService.create(createWallet);
  }

  @Post('/transfer')
  transferMoney(@Body() body): Promise<Wallet> {
    return this.walletService.transferMoney(body);
  }

  @Get()
  findAll(@Query() query): Promise<Wallet[]> {
    return this.walletService.findAll(query.accountId);
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<Wallet> {
    return this.walletService.findOne(+id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateWallet): Promise<UpdateResult> {
    return this.walletService.update(+id, updateWallet);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<DeleteResult> {
    return this.walletService.remove(+id);
  }
}
