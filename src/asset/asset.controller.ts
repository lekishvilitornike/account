import { Controller, Get, Post, Body, Put, Param, Delete, Query } from '@nestjs/common';
import { AssetService } from './asset.service';
import { AccountAssetService } from './services/accountAsset.service';
import { AssetToAccountTypesService } from './services/assetToAccountTypes.service';
import { AssetTypeService } from './services/assetType.service';
import { Asset } from './entities/asset.entity';
import { AssetType } from './entities/assetType.entity';
import { AccountAsset } from './entities/AccountAsset.entity';
import { UpdateResult } from 'typeorm/query-builder/result/UpdateResult';
import { DeleteResult } from 'typeorm/browser';

@Controller('asset')
export class AssetController {
  constructor(private readonly assetService: AssetService,
              private readonly assetTypeService: AssetTypeService,
              private readonly accountAssetService: AccountAssetService,
              private readonly assetToAccountTypesService: AssetToAccountTypesService) {
  }

  @Post()
  create(@Body() createAsset): Promise<Asset> {
    return this.assetService.create(createAsset);
  }

  @Get('all')
  findAll(): Promise<Asset[]> {
    return this.assetService.findAll();
  }

  @Get('types')
  findTypes(): Promise<AssetType[]> {
    return this.assetTypeService.findall();
  }

  @Get('getFavourites')
  async getFavourites(@Query() query: any): Promise<string[]> {
    return this.accountAssetService.getFavourites(query);
  }


  @Get(':id')
  findOne(@Param('id') id: string): Promise<Asset> {
    return this.assetService.findOne(+id);
  }

  @Post('addFavourite')
  async addFavourite(@Body() data): Promise<AccountAsset | string> {
    data.AssetToAccountType = await this.assetToAccountTypesService.getIdByName('favourite');
    const asset = await this.assetService.findIdByName(data.asset);
    if (asset) data.asset = asset; else return 'invalid asset';
    return this.accountAssetService.create(data);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateAsset): Promise<UpdateResult> {
    return this.assetService.update(+id, updateAsset);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<DeleteResult> {
    return this.assetService.remove(+id);
  }
}
