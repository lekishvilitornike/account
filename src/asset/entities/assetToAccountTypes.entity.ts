import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { AccountAsset } from './AccountAsset.entity';

@Entity()
export class AssetToAccountTypes {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => AccountAsset, assets => assets.AssetToAccountType)
  assetToAccounts: AccountAsset[];

  @CreateDateColumn()
  createdAt: Date;
}
