import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Asset } from './asset.entity';

@Entity()
export class AssetType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => Asset, assets => assets.assetType)
  assets: Asset[];

  @CreateDateColumn()
  createdAt: Date;
}
