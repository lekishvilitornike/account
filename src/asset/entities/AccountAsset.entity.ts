import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Asset } from './asset.entity';
import { Account } from '../../account/entities/account.entity';
import { AssetToAccountTypes } from './assetToAccountTypes.entity';
import { OrderEntity } from '../../orders/entities/order.entity';

@Entity()
export class AccountAsset {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ type: 'float', nullable: true })
  public invested: number;

  @Column({ type: 'float', nullable: true })
  public investedPercentage: number;

  @Column({ type: 'float', nullable: true })
  public units: number;

  @Column({ nullable: true })
  public avgOpen: number;

  @ManyToOne(() => AssetToAccountTypes, type => type.assetToAccounts)
  public AssetToAccountType: AssetToAccountTypes;

  @ManyToOne(() => Asset, asset => asset.assetToAccount)
  public asset: Asset;

  @ManyToOne(() => Account, account => account.assetToAccount)
  public account: Account;

  @OneToMany(() => OrderEntity, order => order.orderToAsset, { nullable: true })
  public orders: OrderEntity[];

  @CreateDateColumn()
  createdAt: Date;
}
