import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { AssetType } from './assetType.entity';
import { AccountAsset } from './AccountAsset.entity';
import { OrderEntity } from '../../orders/entities/order.entity';

@Entity()
export class Asset {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ nullable: true })
  exchange: string;

  @ManyToOne(() => AssetType, type => type.assets)
  assetType: AssetType;

  @OneToMany(() => AccountAsset, assetToAccount => assetToAccount.asset)
  assetToAccount: AccountAsset[];

  @OneToMany(() => OrderEntity, assets => assets.assetType)
  orders: OrderEntity[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
