import { Connection, Entity, Repository } from 'typeorm';
import { AccountAsset } from '../entities/AccountAsset.entity';
import { UpdateResult } from 'typeorm/query-builder/result/UpdateResult';
import { AssetToAccountTypesService } from './assetToAccountTypes.service';

@Entity()
export class AccountAssetService {
  private readonly accountAsset: Repository<AccountAsset>;

  constructor(connection: Connection, private readonly assetToAccountTypesService: AssetToAccountTypesService) {
    this.accountAsset = connection.getRepository(AccountAsset);
  }

  create(data: any): Promise<AccountAsset> {
    return this.accountAsset.save(data);
  }

  findAll(data): Promise<AccountAsset[]> {
    return this.accountAsset.find({
      where: {
        account: data.accountId,
      },
      relations: ['asset', 'AssetToAccountType'],
    });
  }

  async getFavourites(data: any): Promise<string[]> {
    return await this.accountAsset.find({
      where: {
        account: data.accountId,
        AssetToAccountType: await this.assetToAccountTypesService.getIdByName('favourite'),
      },
      relations: ['asset'],
    }).then(res => res.map(f => {
      return f.asset.name;
    }));
  }

  async updateOrCreateData(data): Promise<UpdateResult | AccountAsset> {
    const { account, asset } = data;
    const accountAssets = await this.accountAsset.find({ account, asset }).then(x => x[0]);
    const newEntity = {
      account: account,
      invested: 0,
      investedPercentage: 0,
      asset: asset,
      AssetToAccountType: await this.assetToAccountTypesService.getIdByName('trading'),
    };
    if (!accountAssets) return this.create(newEntity);
    let { invested: assetInvested, investedPercentage: assetInvestedPercentage } = accountAssets;
    assetInvested += Number(data.invested);
    assetInvestedPercentage = (assetInvested * 100) / Number(data.balance);
    const entity = { ...accountAssets, invested: assetInvested, investedPercentage: assetInvestedPercentage };
    return this.accountAsset.update(entity.id, entity);
  }
}
