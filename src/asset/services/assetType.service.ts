import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { AssetType } from '../entities/assetType.entity';


@Injectable()
export class AssetTypeService {
  private readonly assetTypeRepository: Repository<AssetType>;

  constructor(connection: Connection) {
    this.assetTypeRepository = connection.getRepository(AssetType);
  }

  findall(): Promise<AssetType[]> {
    return this.assetTypeRepository.find({});
  }

  getTypeByName(name: string): Promise<AssetType> {
    return this.assetTypeRepository.find({ where: { name } }).then(x => x[0]);
  }
}
