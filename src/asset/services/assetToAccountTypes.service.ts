import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { AssetToAccountTypes } from '../entities/assetToAccountTypes.entity';


@Injectable()
export class AssetToAccountTypesService {
  private readonly assetToAccountTypeRepository: Repository<AssetToAccountTypes>;

  constructor(connection: Connection) {
    this.assetToAccountTypeRepository = connection.getRepository(AssetToAccountTypes);
  }

  getIdByName(name: string): Promise<AssetToAccountTypes> {
    return this.assetToAccountTypeRepository.find({ where: { name } }).then(x => x[0]);
  }
}
