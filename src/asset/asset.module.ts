import { Module } from '@nestjs/common';
import { AssetService } from './asset.service';
import { AssetController } from './asset.controller';
import { AssetTypeService } from './services/assetType.service';
import { AccountAssetService } from './services/accountAsset.service';
import { AssetToAccountTypesService } from './services/assetToAccountTypes.service';

@Module({
  controllers: [AssetController],
  providers: [AssetService, AssetTypeService, AccountAssetService, AssetToAccountTypesService],
  exports: [AssetService, AccountAssetService],
})
export class AssetModule {
}
