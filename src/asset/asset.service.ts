import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { Asset } from './entities/asset.entity';
import { DeleteResult } from 'typeorm/browser';
import { UpdateResult } from 'typeorm/query-builder/result/UpdateResult';
import { AssetType } from './entities/assetType.entity';
import { AssetTypeService } from './services/assetType.service';

@Injectable()
export class AssetService {
  private readonly assetRepository: Repository<Asset>;

  constructor(connection: Connection, private readonly assetTypeService: AssetTypeService) {
    this.assetRepository = connection.getRepository(Asset);
  }

  async create(createAsset): Promise<Asset> {
    createAsset.assetType = await this.assetTypeService.getTypeByName(createAsset.assetType);
    return this.assetRepository.save(createAsset);
  }

  findAll(): Promise<Asset[]> {
    return this.assetRepository.find();
  }

  findByParams(params: any): Promise<Asset[]> {
    return this.assetRepository.find({ where: params });
  }

  findIdByName(name: string): Promise<Asset> {
    return this.assetRepository.find({ where: { name } }).then(x => x[0]);
  }

  findOne(id: number): Promise<Asset> {
    return this.assetRepository.findOne(id);
  }

  update(id: number, updateAsset): Promise<UpdateResult> {
    return this.assetRepository.update(id, updateAsset);
  }

  remove(id: number): Promise<DeleteResult> {
    return this.assetRepository.delete(id);
  }
}
